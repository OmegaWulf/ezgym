//
//  MyWorkoutsTVC.swift
//  ezGym
//
//  Created by Nikita Thomas on 12/10/18.
//  Copyright © 2018 Nikita Thomas. All rights reserved.
//

import UIKit
import CoreData

class MyWorkoutsTVC: UITableViewController {

    var managedContext: NSManagedObjectContext!
    
    var workouts: [NSManagedObject] = []
    
    var newWorkoutObject: Workout?
    
    
    @IBAction func newWorkoutButton(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Name of Exercise", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addTextField { (textField) in
            textField.placeholder = "Type exercise name..."
        }
        alert.addAction(UIAlertAction(title: "Create", style: .default, handler: { (action) in
            if let name = alert.textFields?.first?.text, name != "" {
                
                //BUG: I think this is correct: Clear old data from managed context?
                self.managedContext.reset()
                // BUG: Not confident about these next two lines, could be a problem
                self.newWorkoutObject = Workout(context: self.managedContext)
                self.newWorkoutObject?.name = name
        
                
                

                self.performSegue(withIdentifier: "manageToNewWorkout", sender: nil)
            }
        }))
        
        self.present(alert, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            managedContext = appDelegate.persistentContainer.viewContext
        }
        
        pullWorkoutsFromCoreData()
    }
    
    func pullWorkoutsFromCoreData() {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Workout")
        managedContext.reset()
        do {
            workouts = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch workouts from CoreData: \(error)")
        }

        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workouts.count 
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myWorkoutsCell", for: indexPath)
        let workout = workouts[indexPath.row]
        
        let name = workout.value(forKey: "name") as? String
        
        cell.textLabel?.text = name

        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

 

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let workout = workouts[indexPath.row]
            managedContext.delete(workout)
            workouts.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            do {
                try managedContext.save()
            } catch {
                NSLog("Could not delete workout: \(error)")
            }
            
        }
        
    }
  

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */



   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "manageToNewWorkout" {
            guard let dest = segue.destination as? CreateWorkoutVC else { return }
            dest.managedContext = managedContext
            dest.newWorkoutObject = newWorkoutObject
        }
    }


}
