//
//  WorkingOutVC.swift
//  ezGym
//
//  Created by Nikita Thomas on 12/10/18.
//  Copyright © 2018 Nikita Thomas. All rights reserved.
//

import UIKit
import CoreData

class WorkingOutVC: UIViewController {
    
    @IBOutlet weak var workoutNameLabel: UILabel!
    @IBOutlet weak var currentExerciseLabel: UILabel!
    @IBOutlet weak var setsAndRepsLabel: UILabel!
    @IBOutlet weak var currentSetNumberLabel: UILabel!
    @IBOutlet weak var logButtonOutlet: UIButton!
    
    @IBOutlet weak var lastWorkoutStatsLabel: UILabel!
    @IBOutlet weak var overallAverageStatsLabel: UILabel!
    
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var repsTextField: UITextField!
    
    var currentWorkout: Workout!
    var managedContext: NSManagedObjectContext!
    
    var allExercises: [WorkoutExercise] = []
    var completedSets: [WorkoutSet] = []
    
    var currentExercise: WorkoutExercise?
    var currentExerciseSets: Int = 1
    var currentExerciseReps: Int = 1
    var currentExerciseName: String = ""
    
    var lastWorkoutSetsForCurrentExercise: [WorkoutSet] = []
    var lastTenWorkoutSets: [WorkoutSet] = []
    var onSetNumber: Int = 1
    
    @IBAction func LogSetButton(_ sender: UIButton) {
        guard let weight = weightTextField.text, weight != "" else { return }
        guard let reps = repsTextField.text, reps != "" else { return }
        
        completeSet(setNumber: onSetNumber, weight: Double(weight)!, reps: Int(reps)!)
        onSetNumber += 1
        
        if allExercises.count == 1 && onSetNumber > currentExerciseSets {
            // That was the last set completed
            completeWorkout()
            return
        }
        
        updateViews()
        
        
        if onSetNumber <= currentExerciseSets {
            newSet()
        } else {
            // Switching to next exercise
            
            animateLabel(label: currentExerciseLabel)
            allExercises.remove(at: 0)
            print("Removed an exercise. Now have: \(allExercises.count)")
            
            currentExercise = allExercises.first
            getLastSetsForCurrentExercise()
            
            weightTextField.text = ""
            repsTextField.text = ""
            
            onSetNumber = 1
            newSet()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        workoutNameLabel.text = currentWorkout.value(forKey: "name") as? String
        getAllExercises()
        
        newSet()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        weightTextField.becomeFirstResponder()
    }
    
    func updateViews() {
//        weightTextField.text = ""
//        repsTextField.text = ""
        
        currentSetNumberLabel.text = "Set# \(onSetNumber)"
        currentExerciseLabel.text = "\(currentExerciseName)"
        
        if currentExerciseSets > 1 {
            setsAndRepsLabel.text = "\(currentExerciseSets) Sets of \(currentExerciseReps)"
        } else {
            setsAndRepsLabel.text = "\(currentExerciseSets) Set of \(currentExerciseReps)"
        }
        
        if allExercises.count == 1 && onSetNumber == currentExerciseSets {
            // Means we are on the last Set of the last exercise
            print("are on last exercise: \(currentExerciseName) set: \(onSetNumber) out of: \(currentExerciseSets)")
            logButtonOutlet.setTitle("Finish", for: .normal)
        }
        
        
        // Last Set Stats Set #1   -   225lbs   -   11 reps
        let lastSet = lastWorkoutSetsForCurrentExercise.first(where: { $0.setNumber == onSetNumber })
        if let setNumber = lastSet?.setNumber, let weight = lastSet?.weight, let reps = lastSet?.reps {
            lastWorkoutStatsLabel.text = "Set #\(setNumber)   -   \(weight)lbs   -   \(reps) reps"
        } else  {
            lastWorkoutStatsLabel.text = "NA   -   NA   -   NA"
        }
        
        
        // Last 10 sets stats
        let lastTenSet = lastTenWorkoutSets.filter({ $0.setNumber == onSetNumber })
        
        var totalWeight = 0
        var totalReps = 0
        
       
        
        for set in lastTenSet {
            totalReps += Int(set.reps)
            totalWeight += Int(set.weight)
        }
        
        print("totalWeight = \(totalWeight)")
        print("totalreps = \(totalReps)")
        
        if lastTenWorkoutSets.count > 0 {
            let avgWeight = totalWeight / lastTenWorkoutSets.count
            let avgReps = totalReps / lastTenWorkoutSets.count
            
            overallAverageStatsLabel.text = "Set #\(lastTenSet.first?.setNumber ?? Int16(onSetNumber))   -   \(avgWeight)lbs   -   \(avgReps) reps"
        } else {
            overallAverageStatsLabel.text = "NA   -   NA   -   NA"
        }
        
        print(lastTenWorkoutSets.count)
        
//        if let setNumber = lastTenSet.first?.setNumber {
//            overallAverageStatsLabel.text = "Set #\(setNumber)   -   \(avgWeight)lbs   -   \(avgReps) reps"
//        } else  {
//            overallAverageStatsLabel.text = "NA   -   NA   -   NA"
//        }
    }
    
    func getAllExercises() {
        let exerciseSet = currentWorkout.hasWorkoutExercises
        allExercises = Array(exerciseSet!).sorted(by: { ($0 as! WorkoutExercise).order < ($1 as! WorkoutExercise).order }) as! [WorkoutExercise]
    }
    
    var firstSet = true
    
    func newSet() {
        currentExercise = allExercises.first
        
        currentExerciseSets = currentExercise?.value(forKey: "sets") as! Int
        currentExerciseReps = currentExercise?.value(forKey: "reps") as! Int
        currentExerciseName = currentExercise?.value(forKey: "name") as! String
        
        if firstSet {
            getLastSetsForCurrentExercise()
            firstSet = false
        }
        
        
        updateViews()
    }
    

    func completeSet(setNumber: Int, weight: Double, reps: Int) {
        let workoutSet = WorkoutSet(context: managedContext)
        workoutSet.reps = Int16(reps)
        workoutSet.weight = weight
        workoutSet.setNumber = Int16(setNumber)
        workoutSet.timeCompleted = Date()
        
        workoutSet.partOfWorkoutExercise = currentExercise
        
        do {
            try managedContext.save()
        } catch {
            NSLog("Could not save set: \(error)")
        }
        
    }
    
    func getLastSetsForCurrentExercise() {
        // Clear arrays from last exercise first
        lastWorkoutSetsForCurrentExercise = []
        lastTenWorkoutSets = []
        
        let allExerciseSets = Array(currentExercise?.hasWorkoutSets ?? []) as! [WorkoutSet]
        if allExerciseSets.count > 0 {
            let sortedSets = allExerciseSets.sorted(by: { $0.timeCompleted! > $1.timeCompleted!})
            
            // Takes the sets ONLY from the last workout and appends to 'lastWorkoutSetsForCurrentExercise'
            var firstSet = true
            var setNumberOneCount = 0
            
            for set in sortedSets {
                
                if set.setNumber != 1 && firstSet {
                    lastWorkoutSetsForCurrentExercise.append(set)
                    lastTenWorkoutSets.append(set)
                    continue
                }
                
                if firstSet {
                    lastWorkoutSetsForCurrentExercise.append(set)
                    lastTenWorkoutSets.append(set)
                    firstSet = false
                    setNumberOneCount += 1
                    continue
                }
                // Take as many workoutsets with a max of 10 and append to 'lastTenWorkoutSets'
                
                if setNumberOneCount <= 10 {
                    if set.setNumber != 1 {
                        lastTenWorkoutSets.append(set)
                        continue
                    } else  {
                        // On set #1
                        lastTenWorkoutSets.append(set)
                        setNumberOneCount += 1
                        continue
                    }
                } else {
                    return
                }
            }
            
            // Reset firstSet so it runs correctly next time
            firstSet = true
            setNumberOneCount = 0
        } else { return }
        
    }
    
    func completeWorkout() {
        // TODO: Complete the workout
        print("Finished Workout")

        do {
            try managedContext.save()
        } catch {
            NSLog("Couldn't save workout when finishing")
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func animateLabel(label: UILabel) {
        
        label.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIView.AnimationOptions.allowUserInteraction,
                       animations: {
                        label.transform = CGAffineTransform.identity
        },
                       completion: { Void in()  }
        )
    }


}
