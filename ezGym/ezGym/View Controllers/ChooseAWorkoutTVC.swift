//
//  ChooseAWorkoutTVC.swift
//  ezGym
//
//  Created by Nikita Thomas on 12/11/18.
//  Copyright © 2018 Nikita Thomas. All rights reserved.
//

import UIKit
import CoreData

class ChooseAWorkoutTVC: UITableViewController {
    
    var managedContext: NSManagedObjectContext!
    var workouts: [Workout] = []
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            managedContext = appDelegate.persistentContainer.viewContext
        }
        pullWorkoutsFromCoreData()
        setupUIStuff()
    }
    
    func pullWorkoutsFromCoreData() {
        let fetchReq: NSFetchRequest<Workout> = Workout.fetchRequest()
        managedContext.reset()
        do {
            workouts = try managedContext.fetch(fetchReq)
        } catch let error as NSError {
            print("Could not fetch workouts from CoreData: \(error)")
        }
        
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workouts.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chooseWorkoutCell", for: indexPath)
        let workout = workouts[indexPath.row]
        let name = workout.value(forKey: "name") as! String
        
        cell.textLabel?.text = name

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    func setupUIStuff() {
        tableView.separatorStyle = .none
    }


   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toWorkoutView" {
            let dest = segue.destination as! WorkingOutVC
            guard let selectedIndex = tableView.indexPathForSelectedRow else { return }
            let startingWorkout = workouts[selectedIndex.row]
            
            dest.currentWorkout = startingWorkout
            dest.managedContext = managedContext
        }
    }


}
