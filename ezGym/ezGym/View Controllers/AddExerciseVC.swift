//
//  AddWorkoutVC.swift
//  ezGym
//
//  Created by Nikita Thomas on 12/10/18.
//  Copyright © 2018 Nikita Thomas. All rights reserved.
//

import UIKit
import CoreData

class AddExercise: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var setsTextField: UITextField!
    @IBOutlet weak var repsTextField: UITextField!
    var managedContext: NSManagedObjectContext!
    var createWorkoutVC: CreateWorkoutVC?
    var newWorkoutObject: Workout!
    
    var exercises: [Exercise] = []
    var workoutExercise: WorkoutExercise?
    
    @IBAction func removeExerciseButton(_ sender: UIButton) {
        let exercise = exercises[pickerView.selectedRow(inComponent: 0)]
        guard let index = exercises.firstIndex(where: { $0 == exercise }) else { return }
        managedContext.delete(exercise)
        exercises.remove(at: index)
        pickerView.reloadAllComponents()
    }
    
    
    @IBAction func createNewExerciseButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "Name of Exercise", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addTextField { (textField) in
            textField.placeholder = "Type exercise name..."
        }
        alert.addAction(UIAlertAction(title: "Create", style: .default, handler: { (action) in
            if let name = alert.textFields?.first?.text, name != "" {
                let newExercise = Exercise(context: self.managedContext!)
                newExercise.name = name
                do {
                    try self.managedContext?.save()
                } catch {
                    NSLog("Couldn't save context when creating exercise" )
                }
                
                self.exercises.append(newExercise)
                self.exercises.sort(by: { ($0.value(forKey: "name") as! String) < ($1.value(forKey: "name") as! String) })
                
                DispatchQueue.main.async {
                    self.pickerView.reloadAllComponents()
                }
                
            }
        }))
        
        self.present(alert, animated: true)
    }
    
    
    
    @IBAction func addButton(_ sender: Any) {
        let exercise = exercises[pickerView.selectedRow(inComponent: 0)]
        
        guard let sets = setsTextField.text, sets != "" else { return }
        guard let reps = repsTextField.text, reps != "" else { return }
        
        let workoutExercise = WorkoutExercise(context: managedContext)
        workoutExercise.name = exercise.name
        workoutExercise.reps = Int16(reps)!
        workoutExercise.sets = Int16(sets)!
        newWorkoutObject.addToHasWorkoutExercises(workoutExercise)
        
        workoutExercise.order = Int16(createWorkoutVC?.workoutExercises.count ?? 0 + 1)
        
        createWorkoutVC?.workoutExercises.append(workoutExercise)
        createWorkoutVC?.managedContext = managedContext
        
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        pullExerciseList()
        let color = UIColor(red: 153, green: 152, blue: 151, alpha: 1)
        setsTextField.attributedPlaceholder = NSAttributedString(string: "# Sets",
                                                               attributes: [NSAttributedString.Key.foregroundColor: color])
        repsTextField.attributedPlaceholder = NSAttributedString(string: "# Reps",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: color])
        
        UITextField.appearance().keyboardAppearance = .dark
    }
    
    func pullExerciseList() {
        let fetchRequest: NSFetchRequest<Exercise> = Exercise.fetchRequest()
        
        do {
            exercises = try managedContext.fetch(fetchRequest)
            exercises.sort(by: { ($0.value(forKey: "name") as! String) < ($1.value(forKey: "name") as! String) })
        } catch let error as NSError {
            print("Could not fetch exercises from CoreData: \(error)")
        }
        
        for exer in exercises {
            print("Have exercise: \(String(describing: exer.value(forKey: "name")))")
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return exercises.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let exercise = exercises[row]
        return exercise.value(forKey: "name") as? String
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let dest = segue.destination as? CreateWorkoutVC else { return }
        if let workout = workoutExercise {
            dest.workoutExercises.append(workout)
        }
        
    }


}
