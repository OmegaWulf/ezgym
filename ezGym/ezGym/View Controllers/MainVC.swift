//
//  MainVC.swift
//  ezGym
//
//  Created by Nikita Thomas on 12/10/18.
//  Copyright © 2018 Nikita Thomas. All rights reserved.
//

import UIKit
import CoreData

class MainVC: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.isNavigationBarHidden = false
    }
    
    
}
