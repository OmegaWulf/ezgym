//
//  CreateWorkoutVC.swift
//  ezGym
//
//  Created by Nikita Thomas on 12/10/18.
//  Copyright © 2018 Nikita Thomas. All rights reserved.
//

import UIKit
import CoreData

class CreateWorkoutVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var managedContext: NSManagedObjectContext!
    var newWorkoutObject: Workout!
    
    var workoutExercises: [NSManagedObject] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
        print("WorkoutExercises: \(String(describing: workoutExercises))")
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workoutExercises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "createWorkoutCell", for: indexPath)
        
        let exercise = workoutExercises[indexPath.row]
        let name = exercise.value(forKey: "name") as! String
        let sets = exercise.value(forKey: "sets") as! Int
        let reps = exercise.value(forKey: "reps") as! Int
        
        
        cell.textLabel?.text = name
        cell.detailTextLabel?.text = "\(String(sets)) X \(String(reps))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let workoutExercise = workoutExercises[indexPath.row]
            managedContext.delete(workoutExercise)
            workoutExercises.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    
    
    
    @IBAction func finishWorkoutButton(_ sender: UIButton) {
        guard workoutExercises.count > 0 else { return }
        
        do {
            try managedContext.save()
        } catch {
            NSLog("Could not save managedContext RIP: \(error)")
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddNewWorkout" {
            guard let dest = segue.destination as? AddExercise else { return }
            
            dest.createWorkoutVC = self
            dest.managedContext = managedContext
            dest.newWorkoutObject = newWorkoutObject
        }
    }
 

}
